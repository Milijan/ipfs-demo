// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const IPFS = require('ipfs')
const Repo = require('ipfs-repo')
const series = require('async/series')
const Buffer = require('buffer/').Buffer
const rimraf = require('rimraf')

const fs = require('fs')
var fileSize

const ipfsRepoPath = '/ipfsRepo'
var repo

var allowAdding = false
var isLoading = false

var selectedFile
const startBtn = document.getElementById('start_btn')
const stopBtn = document.getElementById('stop_btn')
const resetRepoBtn = document.getElementById('reset_repo_btn')
const dot = document.getElementById('dot')
const addBtn = document.getElementById('add_btn')
const checkBtn = document.getElementById('check_btn')
const ipfsInfo = document.getElementById('ipfs_info')
const loadingImg = document.getElementById('loading')
loadingImg.style.visibility = 'hidden'

let node
initButtonEventListeners()

function startNode() {

    if (node != null) {
        ipfsInfo.innerHTML = 'Node already running.'
        return
    }

    ipfsInfo.innerHTML = 'Starting IPFS node.'

    isLoading = true
    loadingImg.style.visibility = 'visible'

    repo = new Repo(ipfsRepoPath)

    node = new IPFS({
        repo: repo
    })
    node.on('ready', successfulNodeStartHandler)
}

function stopNode(callback) {
    //Bag!! IPFS tim radi na ispravljanju ovoga trenutno, node.stop() ne ubije proces, node ostaje aktivan
    if (!node) {
        return
    }

    node.stop(() => {
        console.log('Node stopped.')
        ipfsInfo.innerHTML = 'Node stopped.'
        //hak...
        node = null
        dot.src = './img/red_dot.png'

        if (callback) {
            callback()
        }
    })
}

function setRepoPath(path) {
    ipfsRepoPath = path
}


function resetRepo() {
    //Deletes repo folder and initializes an empty one

    if (!repo) {
        ipfsInfo.innerHTML = "repo doesn't exist'"
        return
    }

    repo.exists((error, repoExists) => {
        if (error) {
            ipfsInfo.innerHTML = 'Repo folder possibly corrupt. Delete repo folder.'
            return
        }

        if (!repoExists) {
            ipfsInfo.innerHTML = "Repo doesn't exist', creating a new one."
            startNode()

        } else {
            stopNode(() => {

                ipfsInfo.innerHTML = "Deleting existing repo and creating a new one."

                repo.close((error) => {
                    if (error) { ipfsInfo.innerHTML = 'Unable to close and delete existing repo.' }

                    rimraf(ipfsRepoPath, () => {
                        console.log('Successfully deleted repo folder.')
                        startNode()
                    })
                })
            })            
        }
    })
}



function successfulNodeStartHandler() {
    var statusText = 'Hej, upalio se IPFS node.\n'
    console.log(statusText)
    ipfsInfo.innerHTML = statusText;
    dot.src = './img/green_dot.png'
    isLoading = false
    loadingImg.style.visibility = 'hidden'
    printNodeId()
}

function printNodeId() {
    node.id(function (err, response) {
        if (err) {
            throw err
        }
        var statusText = 'My IPFS node ID is: ' + response.id
        console.log(statusText)
        ipfsInfo.innerHTML += '<br>' + statusText
        allowAdding = true
    })
}

function initButtonEventListeners() {

    startBtn.addEventListener('click', () => {
        startNode()
    })

    stopBtn.addEventListener('click', () => {
        stopNode()
    })

    resetRepoBtn.addEventListener('click', () => {
        resetRepo()
    })

    addBtn.addEventListener('click', () => {
        ipfsInfo.innerHTML = ''

        selectedFile = document.getElementById('selected_file').files[0]
        if (!allowAdding || !selectedFile) {
            var statusText = 'Node not running or file not selected'
            ipfsInfo.innerHTML = statusText
            throw (new Error(statusText))
        }

        loadingImg.style.visibility = 'visible'

        addFileToIpfs(selectedFile)
    })

    checkBtn.addEventListener('click', () => {
        ipfsInfo.innerHTML = ''

        var hash = document.getElementById('hash').value
        if (hash.length == 0) {
            var statusText = 'hash cannot be empty'
            ipfsInfo.innerHTML = statusText
            throw (new Error(statusText))
        }

        isLoading = true
        loadingImg.style.visibility = 'visible'

        retrieveFileByHash(hash)
    })
}


function addFileToIpfs(file) {

    if (!node) {
        ipfsInfo.innerHTML = 'Node not running, unable to add file to IPFS.'
        return
    }

    isLoading = true

    function handleProgress(progress) {
        ipfsInfo.innerHTML = (progress / fileSize * 100).toFixed(1) + '%'
        if ((progress / fileSize * 100).toFixed(1) >= 10) {
            node.stop()
        }
    }

    fileSize = fs.statSync(file.path).size

    node.files.add({
        path: file.path,
        content: fs.readFileSync(file.path)
    },
        { progress: handleProgress },
        (err, fileAdded) => {

            isLoading = false
            loadingImg.style.visibility = 'hidden'

            if (err) {
                console.log(err)
                return
            }
            // 
            var statusText = 'Added file at: ' + fileAdded[0].hash;
            console.log(statusText)
            ipfsInfo.innerHTML = statusText
            document.getElementById('selected_file').value = ''
        })
}

function retrieveFileByHash(hash) {
    node.files.cat(hash, (error, data) => {

        isLoading = false
        loadingImg.style.visibility = 'hidden'

        var statusText = ''
        if (error) {
            statusText = 'file with this hash not found'
            console.log(statusText)
            ipfsInfo.innerHTML = statusText
            return
        }

        statusText = 'File found in IPFS!'
        //writing raw data to stdout, no need for it now
        //process.stdout.write(data)
        ipfsInfo.innerHTML = statusText
    })
}
